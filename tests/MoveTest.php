<?php
declare(strict_types=1);
namespace TicTacToeTests;

use PHPUnit\Framework\TestCase;
use TicTacToe\Move;

class MoveTests extends TestCase
{
    /**
     * Makes a move using the $boardState
     * $boardState contains 2 dimensional array of the game
    field
     * X represents one team, O - the other team, empty
    string means field is
    not yet taken.
     * example
     *
     * [
     *
     * y\x 0    1   2   +
     * 0 ['X', 'O', '']
     * 1 ['X', 'O', 'O']
     * 2 ['', '', '']
     * + ]
     * Returns an array, containing x and y coordinates for
    next move, and th
    e unit that now occupies it.
     * Example: [2, 0, 'O'] - upper right corner - O player
     *
     * @param array $boardState Current board state
     * @param string $playerUnit Player unit representation
     *
     * @return array
     */
    public function testMakeMove()
    {
        $boardState = [['X', 'O', ''],
                        ['X', 'O', 'O'],
                        ['X', 'X', 'X']];
        $move = new Move();
        //force the robot to chose the only choice available
        $result = $move->makeMove($boardState, 'O');
        $this->assertEquals([2,0,'O'], $result, "Has to be on upper tright conrner [2, 0]");
    }
}

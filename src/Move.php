<?php
declare(strict_types=1);
namespace TicTacToe;

use PHPUnit\Framework\TestCase;


class Move
{
    /**
     * @param $board
     * @param string $playerUnit
     */
    public function makeMove($board, $playerUnit = 'X')
    {
        //simple strategy
        for($i = 0 ; $i < count($board); $i++)
        {
            $index = array_search('', $board[$i]);
            if($index!==false)
                return [$index, $i, $playerUnit];
        }
        return false;
    }
}